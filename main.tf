locals {
  project_id = "nomadic-rush-408023"
  region = "northamerica-northeast2"
  env = "tst"
  zone = "northamerica-northeast2-b"
}

##################### VPC######################
module "vpc_01" {
  source = "./modules/vpc"
  vpc_name = "${local.project_id}-vpc"
  vpc_description = "VPC network for development environment"
  vpc_auto_create_subnetworks = "false"
  vpc_routing_mode = "GLOBAL"
  project_id = local.project_id
}