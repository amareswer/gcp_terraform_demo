resource "google_compute_network" "vpc" {
  name                            = var.vpc_name
  description                     = var.vpc_description
  auto_create_subnetworks         = var.vpc_auto_create_subnetworks
  routing_mode                    = var.vpc_routing_mode  #Possible values are REGIONAL and GLOBAL
  delete_default_routes_on_create = var.delete_default_internet_gateway_routes 
  project                         = var.project_id
  mtu                             = var.mtu
  enable_ula_internal_ipv6        = var.enable_ipv6_ula
  internal_ipv6_range             = var.internal_ipv6_range
}


/******************************************
	Shared VPC
 *****************************************/
resource "google_compute_shared_vpc_host_project" "shared_vpc_host" {
  provider = google-beta

  count      = var.shared_vpc_host ? 1 : 0
  project    = var.project_id
  depends_on = [google_compute_network.vpc]
}