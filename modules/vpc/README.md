```
module "vpc_01" {
  source = "git::https://gitlab.lblw.ca/aps/terraform-modules.git//modules/vpc_creation"
  vpc_name = "vpc-name"
  vpc_description = "VPC network for development environment"
  vpc_auto_create_subnetworks = "false"
  vpc_routing_mode = "GLOBAL"
  #delete_default_routes_on_create = false 
}
```
