# GitLab CI/CD Configuration for Terraform

This repository contains the GitLab CI/CD YAML configuration file for automating Terraform deployments. The YAML file is designed to work with GitLab CI/CD pipelines, enabling you to easily manage and deploy your infrastructure as code using Terraform.

## Prerequisites

Before using this configuration, make sure you have the following prerequisites:

- A GitLab repository containing your Terraform code.
- A service account JSON key file (`service_account.json`) with the necessary permissions for your infrastructure.

## Configuration

The GitLab CI/CD YAML configuration file (`.gitlab-ci.yml`) in this repository provides a basic setup for automating Terraform deployments. It includes the following stages and jobs:

- **fmt**: Formats the Terraform code using `terraform fmt`.
- **validate**: Validates the Terraform code using `terraform validate`.
- **test**: Performs security scanning on the Terraform code using `tfsec`.
- **plan**: Generates a Terraform execution plan using `terraform plan` and creates an artifact file called `planfile`.
- **apply**: Applies the Terraform changes using `terraform apply`. This job is set to manual triggering and is restricted to the `main` branch.

## Usage

To use this GitLab CI/CD configuration file for your Terraform project, follow these steps:

1. Copy the `.gitlab-ci.yml` file to the root directory of your GitLab repository.
2. Update the file as needed to match your specific Terraform project structure and requirements. For example, you may need to modify environment variables, add additional stages or jobs, or change the branch restrictions.
3. Commit and push the `.gitlab-ci.yml` file to your GitLab repository.
4. GitLab CI/CD will automatically detect the configuration file and start executing the defined stages and jobs whenever changes are pushed to the repository.

## Contributing

If you have suggestions for improving this GitLab CI/CD configuration or encounter any issues, feel free to open an issue or submit a pull request. Contributions are welcome!

## License

This project is licensed under the [MIT License](LICENSE).